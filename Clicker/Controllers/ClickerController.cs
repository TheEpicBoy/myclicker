﻿using Clicker.Base;
using Clicker.Commands;
using Microsoft.AspNetCore.Mvc;

namespace Clicker.Controllers
{
    public class ClickerController : ControllerBase
    {
        private readonly IClickerService _clickerService;
        public ClickerController(IClickerService clickerService)
        {
            _clickerService = clickerService;
        }

        [HttpPost("GetShortUrl")]
        public async Task<IActionResult> GetShortUrlAsync([FromBody]GetShortUrlCommand request)
        {
            var url = await _clickerService.GetShortUrlAsync(request.FullUrl);
            return Ok(url);
        }
        [HttpGet("/{path}")]
        public async Task<IActionResult> RedirectAsync(string path)
        {
            var originalUrl = await _clickerService.GetOriginalUrlAsync(path);
            if (originalUrl == null)
            {
                return NotFound();
            }

            return Redirect(originalUrl);
        }
    }
}
