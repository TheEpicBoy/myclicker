﻿using System.ComponentModel.DataAnnotations;

namespace Clicker.Entities
{
    public sealed class Url
    {
        public Url()
        {
            Guid = Guid.NewGuid();
            CreatedAt = DateTime.UtcNow;
        }
        public Url(string fullUrl, string shortUrl)
        {
            Guid = Guid.NewGuid();
            this.FullUrl = fullUrl;
            this.ShortUrl = shortUrl;
            CreatedAt = DateTime.UtcNow;
        }
        [Key]
        public Guid Guid { get; private set; }
        public string FullUrl { get; set; }
        private string _shortUrl;
        public string ShortUrl
        {
            get => _shortUrl;
            set
            {
                _shortUrl = value;
                SetPath();
            }
        }
        public string Path { get; private set; }
        public int ClickCount { get; private set; } = 0;
        public DateTime CreatedAt { get; }
        public DateTime? LastClickAt { get; private set; }

        public void Click()
        {
            ClickCount++;
            LastClickAt = DateTime.UtcNow;
        }

        private void SetPath()
        {
            var uri = new Uri(ShortUrl);
            var lastSegment = uri.Segments.LastOrDefault();
            Path = lastSegment ?? string.Empty;
        }
    }
}
