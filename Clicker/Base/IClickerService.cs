﻿namespace Clicker.Base
{
    public interface IClickerService
    {
        Task<string> GetShortUrlAsync(string fullUrl);
        Task<string> GetOriginalUrlAsync(string path);
    }
}
