﻿using Clicker.Base;
using Clicker.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Clicker
{
    public static class ServiceCollectionExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IClickerService, ClickerService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ClickerDbContext>(options =>
            options.UseSqlite(configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<DbContext, ClickerDbContext>();
        }
    }
}
