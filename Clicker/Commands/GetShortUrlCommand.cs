﻿namespace Clicker.Commands
{
    public class GetShortUrlCommand
    {
        public string FullUrl { get; set; }
    }
}
