﻿namespace Clicker.Commands
{
    public class GetShortUrlResponse
    {
        public string ShortUrl { get; set;}
    }
}
