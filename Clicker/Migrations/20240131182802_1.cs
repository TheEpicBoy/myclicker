﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clicker.Migrations
{
    /// <inheritdoc />
    public partial class _1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullUrl",
                table: "Urls",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Path",
                table: "Urls",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ShortUrl",
                table: "Urls",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullUrl",
                table: "Urls");

            migrationBuilder.DropColumn(
                name: "Path",
                table: "Urls");

            migrationBuilder.DropColumn(
                name: "ShortUrl",
                table: "Urls");
        }
    }
}
