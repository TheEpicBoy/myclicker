﻿using Clicker.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clicker
{
    public class ClickerDbContext : DbContext
    {
        public ClickerDbContext(DbContextOptions<ClickerDbContext> options)
        : base(options) { }
        
        public DbSet<Url> Urls { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Url>().HasKey(u => u.Guid);
        }
    }
}
