﻿using Clicker.Base;
using Clicker.Commands;
using Clicker.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clicker.Services
{
    public class ClickerService : IClickerService
    {
        private readonly DbContext _context;
        private readonly IHttpContextAccessor _contextAccessor;
        public ClickerService(DbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _contextAccessor = httpContextAccessor;
        }
        public async Task<string> GetShortUrlAsync(string fullUrl)
        {
            var shortUrl = GenerateShortUrl();

            var url = new Url(fullUrl, shortUrl);

             await _context.Set<Url>().AddAsync(url);
            await _context.SaveChangesAsync();

            return shortUrl;

        }

        public async Task<string> GetOriginalUrlAsync(string path)
        {
            var url = await _context.Set<Url>().FirstOrDefaultAsync(url => url.Path == path);

            return url.FullUrl ?? null;

        }

        private string GetCurrentHostUrl()
        {
            var request = _contextAccessor.HttpContext.Request;
            return $"{request.Scheme}://{request.Host}/";
        }

        private string GenerateShortUrl()
        {
            var hostUrl = GetCurrentHostUrl();
            var path = GenerateRandomString();

            return hostUrl + path;
        }

        private string GenerateRandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, random.Next(5,9))
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
